const jwt = require('jsonwebtoken');

// ============================
// Verificar Token 
// ============================

const verificaToken = (req, res, next) => {

    let token = req.get('token');

    jwt.verify(token, process.env.SEED, (err, decoded) => {

        if (err) {
            return res.status(401).json({
                ok: false,
                err
            });
        }

        req.usuario = decoded.usuario;
        next();

    });


};

// ============================
// Verifica ADMIN_ROLE 
// ============================

const verificaAdmin_Role = (req, res, next) => {
    let role = req.usuario.role;
    // console.log(role);

    if (role !== 'ADMIN_ROLE' || !role) {

        return res.status(401).json({
            ok: false,
            err: {
                message: 'EL rol no es administrador'
            }
        });

    }
    next();

}
module.exports = {
    verificaToken,
    verificaAdmin_Role
};