const mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);
var uniqueValidator = require("mongoose-unique-validator");

let Schema = mongoose.Schema;

let rolesValue = {
    values: ['USER_ROLE', 'ADMIN_ROLE'],
    message: '{VALUE} no es un rol valido'
}

let usuarioSchema = new Schema({
    nombre: {
        type: String,
        required: [true, 'El nombre es necesario']
    },
    email: {
        type: String,
        unique: [true, 'Este correo Electronico ya esta registrado'],
        required: [true, 'El correo es necesario']
    },
    password: {
        type: String,
        required: [true, 'La contrasña es necesaria']
    },
    img: {
        type: String,
        required: false
    },
    role: {
        type: String,
        default: 'USER_ROLE',
        enum: rolesValue
    },
    estado: {
        type: Boolean,
        default: true
    },
    google: {
        type: Boolean,
        default: false


    }
});
// modificar el metodo toJSON para que no retorne la contrasenia
usuarioSchema.methods.toJSON = function() {
    let user = this;
    let userObject = user.toObject();
    delete userObject.password;

    return userObject;
};

usuarioSchema.plugin(uniqueValidator, { message: '{PATH} debe de ser unico' });

module.exports = mongoose.model('Usuario', usuarioSchema); // exportar el modelo