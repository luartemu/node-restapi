// =========Requireds==============================
const express = require('express');
const app = express();
const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken');
const { OAuth2Client } = require("google-auth-library");
const client = new OAuth2Client(process.env.CLIENT_ID);
const Usuario = require('../models/usuario')

// ============================================= 

//Aqui realizaremos la logica de validacion del login


app.post('/login', (req, resp) => {

    let body = req.body;

    Usuario.findOne({ email: body.email }, (err, usuarioDB) => {
        // Valida si hay un problema con la base de datos
        if (err) {
            return resp.status(500).json({
                ok: false,
                err
            });
        }
        //  Valida si no hay un usuario con ese correo
        if (!usuarioDB) {
            return resp.status(400).json({
                ok: false,
                err: {
                    message: 'El usuario o contrasenia de base de datos no fue encontrado'
                }
            });

        }
        // Valida la contrasena del usuario 
        if (!bcrypt.compareSync(body.password, usuarioDB.password)) {
            return resp.status(400).json({
                ok: false,
                err: {
                    message: 'El usuario o contrasenia de base de datos no fue encontrado'
                }
            });
        }
        // Generamos el token 
        let token = jwt.sign({
            usuario: usuarioDB
        }, process.env.SEED, { expiresIn: process.env.CADUCIDAD_TOKEN });
        // si todo esta correcto regresa esta respuesta 
        resp.json({
            ok: true,
            usuario: usuarioDB,
            token
        });



    });


});


// Configuraciones de Google 
async function verify(token) {
    const ticket = await client.verifyIdToken({
        idToken: token,
        audience: process.env.CLIENT_ID, // Specify the CLIENT_ID of the app that accesses the backend
        // Or, if multiple clients access the backend:
        //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
    });
    const payload = ticket.getPayload();

    return {
        nombre: payload.name,
        email: payload.email,
        img: payload.picture,
        google: true
    }
    // const userid = payload['sub'];

}


app.post('/google', async(req, resp) => {
    let token = req.body.idtoken;

    let usuarioGoogle = await verify(token)
        .catch(err => {
            return resp.status(403).json({
                ok: false,
                err
            });
        });

    Usuario.findOne({ email: usuarioGoogle.email }, (err, usuarioDB) => {

        if (err) {
            return resp.status(400).json({
                ok: false,
                err
            });
        }

        if (usuarioDB) {
            if (usuarioDB.google === false) {
                return resp.status(400).json({
                    ok: false,
                    err: {
                        message: 'Debe de utilizar el login Normal'
                    }
                });
            } else {
                let token = jwt.sign({ usuario: usuarioDB }, process.env.SEED, {
                    expiresIn: process.env.CADUCIDAD_TOKEN
                });

                return resp.json({
                    ok: true,
                    usuario: usuarioDB,
                    token
                });
            }

        } else {
            let usuario = new Usuario({
                nombre: usuarioGoogle.nombre,
                email: usuarioGoogle.email,
                password: ':)',
                img: usuarioGoogle.img,
                google: usuarioGoogle.google,
            });

            usuario.save((err, usuarioDB) => {
                if (err) {
                    return resp.status(400).json({
                        ok: false,
                        err
                    });
                }
                let token = jwt.sign({
                    usuario: usuarioDB
                }, process.env.SEED, {
                    expiresIn: process.env.CADUCIDAD_TOKEN
                });

                return resp.json({
                    ok: true,
                    usuario: usuarioDB,
                    token
                });

            });
        }


    });









});




module.exports = app;