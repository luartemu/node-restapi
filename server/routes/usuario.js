// requireds
const express = require('express');

const app = express();

const bcrypt = require("bcrypt");

const _ = require('underscore');

const { verificaToken, verificaAdmin_Role } = require('../middlewares/autenticacion')

const Usuario = require('../models/usuario')


// Obtener todos los usuarios 
app.get('/usuario', verificaToken, (req, res) => {

    // return res.json({
    //     usuario: req.usuario,
    //     nombre: req.usuario.nombre,
    //     email: req.usuario.email

    // });

    let desde = req.query.desde || 0;
    desde = Number(desde);

    let limite = req.query.limite || 5; // registra el limite de registros a enviar si no lo manda es 5.
    limite = Number(limite);

    Usuario.find({ estado: true }, 'nombre email role estado google img')
        .skip(desde) // salta la cantidad de registros enviados.
        .limit(limite) // Limita a solo retornar una cantidad de registros.
        .exec((err, usuarios) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }


            Usuario.count({ estado: true }, (err, cuantos) => {

                res.json({
                    ok: true,
                    usuarios,
                    cuantos
                });
            });



        });



});



//  peticion post para guardar un registro 
app.post('/usuario', [verificaToken, verificaAdmin_Role], (req, res) => {

    let body = req.body;

    let usuario = new Usuario({
        nombre: body.nombre,
        email: body.email,
        password: bcrypt.hashSync(body.password, 10),
        role: body.role,
    });

    usuario.save((err, usuarioDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        // usuarioDB.password = null;

        res.json({
            ok: true,
            usuario: usuarioDB
        });

    });


});

// Peticion put para actualizar un registro
app.put('/usuario/:id', [verificaToken, verificaAdmin_Role], (req, res) => {
    let id = req.params.id; // obtener los parametros enviados en la url
    let body = _.pick(req.body, ['nombre', 'email', 'img', 'role', 'estado']); // Escoger solo los parametros que queremos actualizar


    // obtener el usuario Modificado
    let opciones = {
        new: true,
        runValidators: true
    }
    Usuario.findByIdAndUpdate(id, body, opciones, (err, usuarioDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }


        res.json({
            ok: true,
            usuario: usuarioDB
        })


    });
});

app.delete('/usuario/:id', [verificaToken, verificaAdmin_Role], (req, res) => {
    let id = req.params.id;

    let cambiaEstado = {
        estado: false
    }

    Usuario.findByIdAndUpdate(id, cambiaEstado, { new: true }, (err, usuario) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        if (!usuario.estado) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'El usuario no existe'
                }
            });
        }

        res.json({
            ok: true,
            usuario

        });

    });
});

module.exports = app;