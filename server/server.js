// requireds
require('./config/config');
const express = require('express');
const mongoose = require('mongoose');
const app = express();
const path = require('path');
const bodyParser = require("body-parser");


// configurar body parser 

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json

app.use(bodyParser.json())

// habilitar  la carpeta public 
app.use(express.static(path.resolve(__dirname, '../public')));

//  fin de la configuracion


//Configuracion global de rutas 
app.use(require('./routes/index'));


// Conexion a la Base de Datos de MongoDB
mongoose.connect(process.env.URLDB, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
}, (err) => {
    if (err)
        throw new Error(err);
    else {
        console.log(`Base de Datos Conectada`);

    }
});

app.listen(process.env.PORT, () => {
    console.log(`Escuchando en el puerto 3000`);
});